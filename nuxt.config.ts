// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr: false,
    app: {
        head: {
            charset: 'utf-8',
            viewport: 'width=device-width, initial-scale=1',
            title: 'Development SPB'
        }
    },
    runtimeConfig: {
      public: {
        apiBaseURL: 'https://backend.dev-spb.ru/api',
        assetsBaseURL: 'https://backend.dev-spb.ru/api/assets/image/',
      },
    },
    modules: [
      'nuxt-swiper'
    ],
    css: [
        "@/assets/scss/style.scss"
    ],
    vite: {
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@import "~/assets/scss/_variables.scss";'
                },
            },
        },
    },
})
