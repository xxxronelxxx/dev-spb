# Сайт веб-студии Development SPB/TEX Studio (Nuxt+Cockpit CMS)

# Nuxt 3 Minimal Starter
Look at the Nuxt 3 documentation to learn more.

# Setup
Make sure to install the dependencies:

# npm
npm install

# Development Server
Start the development server on http://localhost:3000

npm run dev

# Production
Build the application for production:

npm run build

# Locally preview production build:

npm run preview

# Production generate site:

npm run generate
