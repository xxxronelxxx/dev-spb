export const generateAssetLink = (id, h = 500, q = 500) => {
    const config = useRuntimeConfig()
    return config.public.assetsBaseURL + id + '?m=thumbnail&h=500&q=500&o=1'
  }